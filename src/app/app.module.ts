import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ParentComponent } from './lifecycle-hooks/on-change/parent/parent.component';
import { ChildComponent } from './lifecycle-hooks/on-change/child/child.component';
import { IparentComponent } from './lifecycle-hooks/on-init/iparent/iparent.component';
import { IchildComponent } from './lifecycle-hooks/on-init/ichild/ichild.component';
import { DchildComponent } from './lifecycle-hooks/do-check/dchild/dchild.component';
import { OparentComponent } from './lifecycle-hooks/on-destroy/oparent/oparent.component';

@NgModule({
  declarations: [
    AppComponent,
    ParentComponent,
    ChildComponent,
    IparentComponent,
    IchildComponent,
    DchildComponent,
    OparentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
