import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ParentComponent } from './lifecycle-hooks/on-change/parent/parent.component';
import { IparentComponent } from './lifecycle-hooks/on-init/iparent/iparent.component';
import { DchildComponent } from './lifecycle-hooks/do-check/dchild/dchild.component';
import { OparentComponent } from './lifecycle-hooks/on-destroy/oparent/oparent.component';

const routes: Routes = [
  { path: 'on-change', component: ParentComponent},
  { path: 'on-init', component: IparentComponent },
  { path: 'do-check', component: DchildComponent },
  { path: 'on-destroy', component: OparentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
