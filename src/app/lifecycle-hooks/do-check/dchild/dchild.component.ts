import { ChangeDetectorRef, Component, DoCheck } from '@angular/core';

@Component({
  selector: 'app-dchild',
  template: `
              <h1>Ejemplo de uso del hook ngDoCheck</h1>
              <p>DATO: {{ dato[dato.length - 1] }}</p>
            `
})
export class DchildComponent implements DoCheck{

  lifecycleTicks: number = 0;
  oldTheData: string = '';
  dato: string[] = ['Inicial'];

  constructor(private changeDetector: ChangeDetectorRef) {

    this.changeDetector.detach(); //Permite que la clase realice su propia detección de cambios

    setTimeout(() => {
      this.oldTheData = 'Final'; // Error Intencional
      this.dato.push('Intermedio');
    }, 1000);

    setTimeout(() => {
      this.dato.push('Final');
      this.changeDetector.markForCheck();
    }, 5000);
  }

  ngDoCheck() {

    console.log(++this.lifecycleTicks);

    if (this.dato[this.dato.length - 1] !== this.oldTheData) {
      this.changeDetector.detectChanges();
    }
  }
}
