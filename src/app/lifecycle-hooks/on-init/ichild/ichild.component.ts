import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-ichild',
  template:  `
                <h3>Componente Hijo</h3>
                <p>MARCAS: {{lifecycleTicks }}</p>
                <p>DATO: {{ dato }}</p>
             `
})
export class IchildComponent implements OnInit{

  @Input() dato: string = '';
  lifecycleTicks: number = 0;

  ngOnInit() {
    this.lifecycleTicks++;
    console.log('Cambio cada 5ms... ' + this.lifecycleTicks);
  }
}
