import { Component } from '@angular/core';

@Component({
  selector: 'app-iparent',
  template: `
              <h1>Ejemplo usando el hook OnInit</h1>
              <app-child [dato]="datoArbitrario"></app-child>
            `
})
export class IparentComponent {

  datoArbitrario: string = 'Inicial';

  constructor() {
    setTimeout(() => {
      this.datoArbitrario = 'Final';
    }, 5000);
    console.log(this.datoArbitrario);
  }
}
