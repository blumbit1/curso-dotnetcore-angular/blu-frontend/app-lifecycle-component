import { Component, Directive, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appDestroyListener]'
})
export class DestroyListenerDirective implements OnDestroy {

  ngOnDestroy() {
    console.log("Adiós mundo cruel !!!");
  }
}

@Component({
  selector: 'app-oparent',
  template: `
              <h1>Ejemplo de uso del hook ngOnDestroy</h1>
              <button (click)="toggleDestroy()">Destrucción alterna!</button>
              <p appDestroyListener *ngIf="destroy">Puedo ser destruido!</p>
            `
})
export class OparentComponent {

  destroy: boolean = true;

  toggleDestroy() {
    this.destroy = !this.destroy;
  }
}
