import { Component } from '@angular/core';

@Component({
  selector: 'app-parent',
  template: `
              <h1>Ejemplo usando el hook OnChange</h1>
              <app-child [dato]="datoArbitrario"></app-child>
            `
})
export class ParentComponent {

  datoArbitrario: string = 'Inicial';

  constructor() {
    setTimeout(() => {
      this.datoArbitrario = 'Final';
    }, 5000);
    console.log(this.datoArbitrario);
  }
}
