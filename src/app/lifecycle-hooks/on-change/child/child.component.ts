import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-child',
  template:  `
              <h3>Componente Hijo</h3>
              <p>MARCAS: {{ lifecycleTicks }}</p>
              <p>DATO: {{ dato }}</p>
  `
})
export class ChildComponent implements OnChanges{

  // Para comunicarnos con un componente externo, utilizamos el decorador @Input
  @Input() dato: string = '';
  lifecycleTicks: number = 0;

  ngOnChanges(): void {
      this.lifecycleTicks++;
      console.log('Cambio cada 5ms... ' + this.lifecycleTicks);
  }
}
